<?php

class Kategoriak_osztaly extends MY_Model{
	
	public function kategoriaLista($limit = 11, $order = '') {
		$ci = getCI();		if ($order != '') $order = ' ORDER BY '.$order;
		$lista = $ci->Sql->gets("kategoriak", $order.' LIMIT '.$limit);
		return $lista;
	}
	public function kategoriaFa() {
		$ci = getCI();
		$lista = $ci->Sql->kategoriaFa(0);
		foreach($lista as $k => $v) {
			$lista[$k]->termekdb = $ci->Sql->getFieldValue("SELECT COUNT(*) as ossz FROM termekxkategoria WHERE kategoria_id = {$v->id} ", 'ossz');
			
		}
		
		return $lista;
	}
	
	public function gyermekKategoriak($kategoriaId = 0) {
		return $this->gets('kategoriak', ' WHERE szulo_id = '.$kategoriaId." ORDER BY nev ASC");
	}
	
	public function kategoriaSzegmens($szegmens) {
		$kategoria = $this->sqlSor("SELECT * FROM kategoriak WHERE slug = '$szegmens' LIMIT 1");
		
		if($kategoria) {
			return $kategoria;
		} else {
			return false;
		}
	}
}
