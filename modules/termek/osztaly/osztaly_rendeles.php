<?php
include_once ('osztaly_termek.php');
		
class Rendeles_osztaly extends MY_Model {
	
	var $termekLista = array();
	var $koltsegLista = array();
	
	var $osszNetto = null;
	var $osszAfa = null;
	var $osszBrutto = null;
	
	var $kosarOsszNetto = null;
	var $kosarOsszAfa = null;
	var $kosarOsszBrutto = null;
	
	var $armodositoArNetto = array();
	var $armodositoArBrutto = array();
	
	var $armodositok = null;
	var $vevo = null;
	
	var $ervenyesitettArmodositok = array();
	
	function betoltesMegrendeles($id) {
		$rs = $this->get($id, 'rendelesek', 'id');
		if(!$rs) return false;
		
		foreach($rs as $k => $v) {
			$this->$k = $v;
		}
		$termekidArr = $this->gets("rendeles_termekek", " WHERE rendeles_id = $id ");
		if($termekidArr) {
			foreach($termekidArr as $termek) {
				$this->termekLista[] = new Termek_osztaly($termek->id, true);
			}
		}
		$this->modositoNevek = array('szallitasmod' => 'Szállítási mód', 'fizetesmod' => 'Fizetési mód', 'kedvezmeny' => 'Kedvezmény', 'egyeb' => 'Egyéb költség');
		
		$this->armodositok = $this->gets("rendeles_armodositok", " WHERE rendeles_id = $id ");
		if($this->armodositok) foreach($this->armodositok as $k => $sor) {
			
			$this->armodositok[$k]->megnevezes = $this->modositoNevek[$sor->tipus]; 			$this->{$sor->tipus} = $this->armodositok[$k];
		}
		
		$this->vevo = $this->get($this->rendeles_felhasznalo_id, "rendeles_felhasznalok", 'id');
	}
	
	function megrendelesAdatTablak($file='') {
		if($file == '' )$file = 'rendelesadatok.php';
		$file = FCPATH.TEMAMAPPA.'/rendszerlevelek/'.$file;
		if(!file_exists($file)) return "Hiányzó rendelésadat template file: ".$file;
		ob_start();include($file);$out = ob_get_contents();ob_end_clean();
		return $out;
	}
	function megrendelesOsszar() {
		if(is_null($this->osszNetto)) $this->megrendelesArszamitas();
		return $this->osszNetto;
	}
	function megrendelesOsszarBrutto() {
		if(is_null($this->osszNetto)) $this->megrendelesArszamitas();
		return $this->osszBrutto;
	}
	function megrendelesOsszarAfa() {
		if(is_null($this->osszNetto)) $this->megrendelesArszamitas();
		return $this->osszAfa;
	}
	function megrendelesArszamitas() {
		
		// termék összárak
		$osszar = 0; 
		$osszarBrutto = 0; 
		foreach($this->termekLista as $t) {
			$osszar += $t->megrendeltOsszAr(); 
			$osszarBrutto += $t->megrendeltOsszBruttoAr();
		}
		
		// ha a bruttóár nagyobb mint a limit akkor nincs költség
			$termekOsszar = $osszar;
			
			if($this->armodositok) {
				foreach($this->armodositok as $modosito) {					
					if(isset($modosito->id)) {
						if($modosito->ingyeneslimitar<$termekOsszar) {
							continue;
						}
						
						$amNetto =  $modosito->ar;
						$amBrutto =  $amNetto+($amNetto/100)*$modosito->afa;
							
						if($modosito->mukodesimod==0) {
							// hozzáadódik az ár
							$amNetto =  $modosito->ar;
							$amBrutto =  $amNetto+($amNetto/100)*$modosito->afa;
							
							$osszar += $amNetto;
							$osszarBrutto += $amBrutto;
							
						} else {
							// százalékos működés							
							$amNetto =  $osszar+($osszar/100)*$modosito->ar;

							$amBrutto =  $amNetto+($amNetto/100)*$modosito->afa;

							
							
							$osszar = $osszar+($osszar/100)*$modosito->ar;
							$osszarBrutto = $osszarBrutto+($osszarBrutto/100)*$modosito->ar;
							
						}						$this->ervenyesitettArmodositok[] = array(
							'nev' => $modosito->nev,
							'netto' => $amNetto,
							'brutto' => $amBrutto,
							'afaertek' => $amBrutto-$amNetto,
							'afa' => $modosito->afa,
						);
						//print $modosito->nev." ".$osszar.' '.$osszarBrutto.'<br>';
					} else {
						
					}
				}
			}
			
		
		
		$this->osszNetto = $osszar;
		$this->osszBrutto = $osszarBrutto;
		$this->osszAfa = $osszarBrutto-$osszar;
                            
		
	}
	function betoltesMunkamenetbol($kosaradatok) {
		
		if(isset($kosaradatok['termekek']))if(!empty($kosaradatok['termekek'])) {
			foreach($kosaradatok['termekek'] as $kosarElem) {
				if((int)$kosarElem['db']==0) continue;
				$termek = new Termek_osztaly($kosarElem['termek_id']);
				$termek->valtozatBeallitas(isset($kosarElem['valtozat'])?$kosarElem['valtozat']:false);
				$termek->valtozatBeallitas2(isset($kosarElem['valtozat2'])?$kosarElem['valtozat2']:false);
				$termek->kosarId = $kosarElem['kosarId'];
				
				if(!empty($kosarElem['opciok'])) {
					foreach($kosarElem['opciok'] as $opcio) {
						$termek->opcioBeallitas($opcio['termek_armodositok_id']);
					}
				}
				$termek->darabszamBeallitas($kosarElem['db']);
				$this->termekLista[] = $termek;
			}
			if(isset($kosaradatok['szallitasmod'])) {
				$this->armodositok['szallitasmod'] = $this->Sql->get((int)$kosaradatok['szallitasmod'], 'szallitasmodok', 'id');
			}
			
			if(isset($kosaradatok['fizetesmod'])) {
				$this->armodositok['fizetesmod'] = $this->Sql->get((int)$kosaradatok['fizetesmod'], 'fizetesmodok', 'id');
			}
			
		}
		
	}
	function arSzamitas() {
		$netto = 0;
		$afa = 0;
		$brutto = 0;
		if(!empty($this->termekLista)) {
			foreach($this->termekLista as $termek) {
				$netto += $termek->kosarOsszNettoAr();
				$afa += $termek->kosarOsszAfa();
				$brutto += $termek->kosarOsszBruttoAr();
			}
			$this->osszAfa = $afa;
			$this->osszNetto = $netto;
			$this->osszBrutto = $brutto;
		}
		return 0;
	}
	function osszNetto() {
		if(is_null($this->osszNetto)) {
			$this->arSzamitas();
		}
		return $this->osszNetto;
	}
	
	
	function osszAfa() {
		if(is_null($this->osszNetto)) {
			$this->arSzamitas();
		}
		return $this->osszAfa;
	}
	
	function osszBrutto() {
		if(is_null($this->osszNetto)) {
			$this->arSzamitas();
		}
		return $this->osszBrutto;
	}
	
	function kosarOsszNetto() {
		if(is_null($this->kosarOsszNetto)) {
			$this->kosarArSzamitas();
		}
		return $this->kosarOsszNetto;
	}
	
	
	function kosarOsszAfa() {
		if(is_null($this->kosarOsszNetto)) {
			$this->kosarArSzamitas();
		}
		return $this->kosarOsszAfa;
	}
	
	function kosarOsszBrutto() {
		if(is_null($this->kosarOsszNetto)) {
			$this->kosarArSzamitas();
		}
		return $this->kosarOsszBrutto;
	}
	function kosarArSzamitas() {
		
		// termék összárak
		$osszar = 0; 
		$osszarBrutto = 0; 
		foreach($this->termekLista as $t) {
			$osszar += $t->kosarOsszNettoAr(); 
			$osszarBrutto += $t->kosarOsszBruttoAr();
		}
		
		// ha a bruttóár nagyobb mint a limit akkor nincs költség
			$termekOsszar = $osszar;
			if($this->armodositok) {
				foreach($this->armodositok as $k => $modosito) {
					if(!isset($modosito->id)) continue;
					if($modosito->ingyeneslimitar<$termekOsszar) {
						$this->armodositoArNetto[$k] = 0;
						$this->armodositoArBrutto[$k] = 0;
						continue;
					}
					
					$amNetto =  $modosito->ar;
					$amBrutto =  $amNetto+($amNetto/100)*$modosito->afa;
						
					if($modosito->mukodesimod==0) {
						// hozzáadódik az ár
						$amNetto =  $modosito->ar;
						$amBrutto =  $amNetto+($amNetto/100)*$modosito->afa;
						$this->armodositoArNetto[$k] = $amNetto;
						$this->armodositoArBrutto[$k] = $amBrutto;
						
						
						$osszar += $amNetto;
						$osszarBrutto += $amBrutto;
						
					} else {
						// százalékos működés
						$osszar = $osszar+($osszar/100)*$modosito->ar;
						$osszarBrutto = $osszarBrutto+($osszarBrutto/100)*$modosito->ar;
						
						$this->armodositoArNetto[$k] = ($osszar/100)*$modosito->ar;
						$this->armodositoArBrutto[$k] = ($osszarBrutto/100)*$modosito->ar;
						
					}
					//print $modosito->nev." ".$osszar.' '.$osszarBrutto.'<br>';
				}
			}
			
		
		
		$this->kosarOsszNetto = $osszar;
		$this->kosarOsszBrutto = $osszarBrutto;
		$this->kosarOsszAfa = $osszarBrutto-$osszar;
                            
		
	}
	function armodositoArNetto( $tipus) {
		if(empty($this->armodositoArNetto)) {
			$this->kosarArSzamitas();
		}
		if(!isset($this->armodositoArNetto[$tipus])) {
			return 0;
		}
		return $this->armodositoArNetto[$tipus];
	}
	
	
	function armodositoArBrutto( $tipus) {
		if(empty($this->armodositoArBrutto)) {
			$this->kosarArSzamitas();
		}
		if(!isset($this->armodositoArBrutto[$tipus])) {
			return 0;
		}
		return $this->armodositoArBrutto[$tipus];
	}
	
	
	
}
