<?php

class Termek_admin extends MY_Modul{
	var $data = array();
	
	public function __construct() {
		parent::__construct();
		include_once('osztaly/osztaly_termeklista.php');
	}
	public function termeklistasliderlista_beallito ($param, $ALG) {
		globalisMemoria("Nyitott menüpont",'Termékek');
		$doboz = $ALG->ujDoboz();
		$opciok = array();
		$cimkek = $this->Sql->gets("termek_cimkek", " ORDER BY nev ASC");
		if($cimkek) foreach($cimkek as $cimke) $opciok[$cimke->id] = $cimke->nev." ({$cimke->nyelv}) ";
		
		$select = new Legordulo(array('felirat' => 'Terméklista típusa', 'nevtomb' => 'a', 'mezonev' => 'tipus', 'opciok' => $opciok, 'ertek' => @$param['tipus']));
		$input = new Szovegmezo(array('felirat' => 'Megjeleníthető termékek száma (pl. 10)', 'nevtomb' => 'a', 'mezonev' => 'termek_db',  'ertek' => isset($param['termek_db'])?$param['termek_db']:12));
		
		$doboz->duplaInput($select, $input);
	}
	
	public function termeklistacimketermeklista_ellenorzes  ($adat) {
		
		$param = unserialize($adat['parameter']);
		
		$elem = $lista = $this->ci->Sql->get( $param['cimke_id'], "termek_cimkek", " id ");
		if(isset($elem->nev)) $adat['elemnev'] = $elem->nev.' címke terméklistája'; else return false;
		return $adat;
		
	}
	
	public function termeklistacimketermeklista_beallito  ($param, $ALG) {
		globalisMemoria("Nyitott menüpont",'Termékek');
		$doboz = $ALG->ujDoboz();
		$opciok = array();
		$cimkek = $this->Sql->gets("termek_cimkek", " ORDER BY nev ASC");
		if($cimkek) foreach($cimkek as $cimke) $opciok[$cimke->id] = $cimke->nev." ({$cimke->nyelv}) ";
		$select = new Legordulo(array('felirat' => 'Terméklista típusa', 'nevtomb' => 'a', 'mezonev' => 'cimke_id', 'opciok' => $opciok, 'ertek' => @$param['cimke_id']));
		
		$doboz->szimplaInput($select);
		$input1 = new Szovegmezo(array('attr'=> ' type="number" ', 'nevtomb'=>'a', 'mezonev' => 'talalatszam', 'felirat' => 'Megjelenítendő termékek száma', 'ertek' => @$param['talalatszam']));
		$doboz->szimplaInput($input1);
		
	}
	public function valtozatesopcio() {
		
		$this->data['tid'] = $id = (int)$this->ci->uri->segment(4);
		$this->data['opc'] = false;
		
		
		$opc = $this->sqlSorok("SELECT * FROM termek_armodositok WHERE termek_id = $id ORDER BY tipus ASC, sorrend ASC");

		if(isset($_POST['termekOpcioHozzadas']))if($_POST['termekOpcioHozzadas']!='0') {
			$sql = "SELECT * FROM termek_armodositok WHERE termek_id = ".(int)$_POST['termekOpcioHozzadas']." ORDER BY tipus ASC, sorrend ASC";
			$opc = $this->sqlSorok($sql);
			unset($_POST['opc']);
			if($opc) foreach($opc as $k => $v) $opc[$k]->id = 0;
		}
		
		if(!$opc) $opc = array();
		if(isset($_POST['opc'])) {
			if($_GET['tipus']=='2') unset($_POST['opc'][0]);
			$opciok = array();
			foreach($_POST['opc'] as $postOpcio) if($postOpcio['id']=='0') $opciok[] = (object)$postOpcio;
			
			$this->data['opc'] = array_merge($opciok, $opc);
		} else {
			$this->data['opc'] = $opc;
		}
		$sql = "SELECT t.id, j.ertek_2 as nev, t.* FROM `jellemzok` j, termekek t WHERE  j.termek_id = t.id AND j.termek_jellemzo_id = ".beallitasOlvasas("termeknev.termekjellemzo_id")." GROUP BY t.id  ORDER BY nev ASC";
		$this->data['termeklista'] = $this->sqlSorok($sql);
		
		return  $this->ci->load->view(ADMINTEMPLATE.'html/opcioszerkeszto', $this->data, true);
	}
	public function keszletek() {
		$w = '';
		globalisMemoria("Nyitott menüpont",'Termékek');
		globalisMemoria('utvonal', array(array('felirat' => 'Termékkészlet')));
		$ALG = new Adminlapgenerator;
		
		$ALG->adatBeallitas('lapCim', "Termékkészlet");
		$ALG->adatBeallitas('szelessegOsztaly', "full-width");
		$ALG->adatBeallitas('fejlecGomb', array('url' => ADMINURL.'termek/keszletszerkesztes/0', 'felirat' => 'Új darabszámok'));
		
		$ALG->tartalomDobozStart();
		
		// kereshető lapozható terméklista
		// keresések
		$sr = $this->ci->input->get('sr');
		
		$keresestorles = false;
		if(isset($sr['keresoszo'])) if($sr['keresoszo']!='') {
			
			$mod = (int)$sr['keresomezo'];
			if($mod==0) $w = ' t.cikkszam LIKE "%'.$sr['keresoszo'].'%" ';
			if($mod==1) $w = ' j.termek_id = t.id AND j.ertek_2 LIKE "%'.$sr['keresoszo'].'%" ';
			if($mod==2) $w = ' j.termek_id = t.id AND j.ertek_3 LIKE "%'.$sr['keresoszo'].'%" ';
			$sql = "SELECT DISTINCT(t.id) FROM termekek t, jellemzok j WHERE $w";
			$idArr = ws_valueArray($this->Sql->sqlSorok($sql), 'id');
			
			if($idArr) {
				$w = "  t.id IN (".implode(',', $idArr).") AND ";
				
			} else {
				$tabla = $ALG->ujTablazat();
				$tabla->keresoTorles();
				redirect(ADMINURL."termek/keszletek?m=".urlencode("Nincs a keresésnek megfelelő találat!"));
				return;
			}
		}
		
		// táblázat adatok összeállítása
		$adatlista = array();
		$start = 0;
		if(isset($_GET['start'])) $start = (int)$_GET['start'];
		$limit = beallitasOlvasas('admin.lapozo.limit');
		$sql = "SELECT COUNT( DISTINCT(t.id)) as ossz FROM `jellemzok` j, termekek t WHERE  $w j.termek_id = t.id AND j.termek_jellemzo_id = ".beallitasOlvasas("termeknev.termekjellemzo_id")." ";
		$osszSorok = $this->sqlSor($sql);
		if($start>$osszSorok->ossz) $start = 0;
		$sql = "SELECT t.id, j.ertek_2 as nev, t.* FROM `jellemzok` j, termekek t WHERE  $w j.termek_id = t.id AND j.termek_jellemzo_id = ".beallitasOlvasas("termeknev.termekjellemzo_id")." GROUP BY t.id  ORDER BY nev ASC LIMIT $start, $limit";
		//print $sql;
		$lista = $this->sqlSorok($sql);
		
		foreach($lista as $k => $sor) {
			$termek = new Termek_osztaly($sor->id);
			$sor->nev = $termek->jellemzo('Név');
			$adatlista[] = $sor;
			$valtozatTipusok = $this->Sql->sqlSorok("SELECT DISTINCT(tipus) FROM termek_armodositok WHERE termek_id = {$sor->id} AND tipus != 1 ");
			$lista[$k]->valtozatszam = count($valtozatTipusok);
		}
		// táblázat beállítás
		$tablazat = $ALG->ujTablazat();
		
		$keresoMezok = array(
			array('felirat' => 'Cikkszám', 'mezonev' => 'cikkszam'),
			array('felirat' => 'Terméknév', 'mezonev' => 'nev'),
			array('felirat' => 'Leírás', 'mezonev' => 'leiras'),
		);
		
		$tablazat->adatBeallitas('keresoMezok', $keresoMezok);
		$tablazat->adatBeallitas('szerkeszto_url', 'termek/keszletszerkesztes/');
		$tablazat->adatBeallitas('megjelenitettMezok', array('nev' => 'Név', 'cikkszam' => 'Cikkszám', 'valtozatszam' => 'Változatoktípusok',  'szerkesztes' => 'Készlet szerkesztés'));
		$tablazat->adatBeallitas('lista', $adatlista);
		
		$tablazat->lapozo($start, $limit, $osszSorok->ossz);
		
		$ALG->tartalomDobozVege();
		
		return $ALG->kimenet();
	}
	public function lista() {
		globalisMemoria("Nyitott menüpont",'Termékek');
		$w = '';
		$sr = $this->ci->input->get('sr');
		globalisMemoria('utvonal', array(array('felirat' => 'Termékek listája')));
		$ALG = new Adminlapgenerator;
		
		// keresések
		$keresestorles = false;
		if(isset($sr['keresoszo'])) if($sr['keresoszo']!='') {
			
			$mod = (int)$sr['keresomezo'];
			if($mod==0) $w = ' t.cikkszam LIKE "%'.$sr['keresoszo'].'%" ';
			if($mod==1) $w = ' j.termek_id = t.id AND j.ertek_2 LIKE "%'.$sr['keresoszo'].'%" ';
			if($mod==2) $w = ' j.termek_id = t.id AND j.ertek_3 LIKE "%'.$sr['keresoszo'].'%" ';
			
			$sql = "SELECT DISTINCT(t.id) FROM termekek t, jellemzok j WHERE $w";
			
			if($mod==3) {
				$w = ' t.id = x.termek_id and x.cimke_id = c.id AND c.nev LIKE "%'.$sr['keresoszo'].'%" ';
				$sql = "SELECT DISTINCT(t.id) FROM termekek t, termek_cimkek c, termekxcimke x WHERE $w";
			
			}
			$idArr = ws_valueArray($this->Sql->sqlSorok($sql), 'id');
			
			if($idArr) {
				$w = "  t.id IN (".implode(',', $idArr).") AND ";
				
			} else {
				$tabla = $ALG->ujTablazat();
				$tabla->keresoTorles();
				redirect(ADMINURL."termek/lista?m=".urlencode("Nincs a keresésnek megfelelő találat!"));
				return;
			}
		}
		
		
		
		
		$ALG->adatBeallitas('lapCim', "Termékek");
		$ALG->adatBeallitas('szelessegOsztaly', "full-width");
		$ALG->adatBeallitas('fejlecGomb', array('url' => ADMINURL.'termek/szerkesztes/0', 'felirat' => 'Új termék'));
		
		$ALG->tartalomDobozStart();
		
		// táblázat adatok összeállítása
		$adatlista = array();
		$start = 0;
		
		$sql = "SELECT t.id, j.ertek_2 as nev, t.* FROM `jellemzok` j, termekek t WHERE  $w j.termek_id = t.id AND j.termek_jellemzo_id = ".beallitasOlvasas("termeknev.termekjellemzo_id")." GROUP BY t.id  ORDER BY nev ASC";
		
		$lista = $this->sqlSorok($sql);
		
		foreach($lista as $sor) {
			$termek = new Termek_osztaly($sor->id);
			$sor->nev = $termek->jellemzo('Név');
			$sor->masolas = '<a onclick="if(!confirm(\'Biztosan?\')) return false;" href="'.ADMINURL.'termek/klonozas/'.$sor->id.'">Klónozás</a>';
			$adatlista[] = $sor;
		}
		// táblázat beállítás
		$tablazat = $ALG->ujTablazat();
		
		$keresoMezok = array(
			array('felirat' => 'Cikkszám', 'mezonev' => 'cikkszam'),
			array('felirat' => 'Terméknév', 'mezonev' => 'nev'),
			array('felirat' => 'Leírás', 'mezonev' => 'leiras'),
			array('felirat' => 'Cimke', 'mezonev' => 'cimke'),
		);
		
		$tablazat->adatBeallitas('keresoMezok', $keresoMezok);
		$tablazat->adatBeallitas('szerkeszto_url', 'termek/szerkesztes/');
		$tablazat->adatBeallitas('torles_url', 'termek/torles/');
		$tablazat->adatBeallitas('megjelenitettMezok', array('nev' => 'Név', 'cikkszam' => 'Cikkszám',  'szerkesztes' => 'Szerkesztés',  'masolas' => 'Klónozás','torles' => 'Törlés' ));
		$tablazat->adatBeallitas('lista', $adatlista);
		// táblázat beállítás vége
		$ALG->tartalomDobozVege();
		
		return $ALG->kimenet();
		
	}
	
	public function torles() {
		
		
		$ci = getCI();
		$id = (int)$ci->uri->segment(4);
		
		$ci->db->query("DELETE FROM termekxkategoria WHERE termek_id = $id");
		$ci->db->query("DELETE FROM termek_armodositok WHERE termek_id = $id");
		$ci->db->query("DELETE FROM jellemzok WHERE termek_id = $id");
		
		
		include_once('osztaly/osztaly_termekkep.php');
		$tk = new Termekkep_osztaly();
		$tk->osszesKepTorlese($id);
		
		$ci->db->query("DELETE FROM termekek WHERE id = $id");
		
		redirect(ADMINURL.'termek/lista?m='.urlencode("Törlés sikeres"));
	}
	public function keszletszerkesztes() {
		globalisMemoria("Nyitott menüpont",'Termékek');
		$this->data['tid'] = $id = (int)$this->ci->uri->segment(4);
		$termek = $this->data['termek'] = new Termek_osztaly($id);
		$valtozatTipusok = $this->Sql->sqlSorok("SELECT DISTINCT(tipus) FROM termek_armodositok WHERE tipus != 1 ");
		$tipusok = array();
		foreach($valtozatTipusok as $sor) $tipusok[] = $sor->tipus;
		$redirect = false;
		// módosítás
		if(is_array($this->ci->input->post('db'))) {
			$arr = $this->ci->input->post('db');
			foreach($arr as $keszlet_id => $db) {
				$a = array('id' => $keszlet_id, 'keszlet' => $db);
				$this->Sql->sqlUpdate($a, 'termek_keszletek');
			}
			$redirect = true;
		}
		
		// új darabszámok felvitele
		if(is_array($this->ci->input->post('ujdb'))) {
			$arr = $this->ci->input->post('ujdb');
			
			
			
			foreach($arr as $amod_ids => $db) {
				if($db == 0) continue;
				$amod_ids = explode('_',$amod_ids);
				$sql = "INSERT INTO termek_keszletek SET keszlet = $db, termek_id = $id , ";
				$w = array();
				foreach($amod_ids as $k => $amodid) {
					
					$w[] =  " valtozat".($k+1)."_id = ".$amodid." ";
					
				}
				$sql .= implode(' , ', $w);
				$this->ci->db->query( $sql) ;
			}
			$redirect = true;
		}
		if($redirect) {
			redirect(ADMINURL.'termek/keszletek');
			return;
		}
		
		globalisMemoria('utvonal', array(array('felirat' => 'Készletlista', 'url' => 'termek/keszletek' ), array('felirat' => 'Termékkészlet szerkesztése')));
		
		$ALG = new Adminlapgenerator;
		
		$ALG->adatBeallitas('lapCim', "Készlet szerkesztése");
		$ALG->adatBeallitas('fejlecGomb', array('url' => ADMINURL.'termek/keszletek', 'felirat' => 'Vissza'));
		
		$ALG->urlapStart(array('attr' => 'method="post" id="termekForm" class="termekForm"'));
		
		$ALG->tartalomDobozStart();
		
		$doboz = $ALG->ujDoboz();
		$doboz->dobozCim($termek->jellemzo('Név')." készlet megadása");
		$tablazat = $ALG->ujTablazat();
		if(empty($tipusok)) {
			$adatlista = array((object)array(
				'id' => 0, 
				'nev' => $termek->jellemzo('Név'), 
				'db' => '<div class="quantity clearfix">
							<a onclick="aJs.keszletNoveles(this, -1);" href="javascript:void(0);" title="" class="btn btn-small decrease"></a>
							<input type="text" name="" value="999">
							<a onclick="aJs.keszletNoveles(this, 1);" href="javascript:void(0);" title="" class="btn btn-small increase"></a>
						</div>' ));
		} else {
			$valtozatSorok = array();
			foreach($tipusok as $tipus_id) {
				$valtozatSorok[] = $this->Sql->sqlSorok("SELECT * FROM termek_armodositok WHERE termek_id = $id AND tipus = $tipus_id ORDER BY nev ASC ");
			}
			// rekurzív tömbflépítés a változatok permutálásával...
			$adatlista = $this->kombinaciosLista($valtozatSorok);
			
			foreach($adatlista as $kindex => $sor) {
				$adatlista[$kindex]->id = 0;
				
				$sql = "SELECT * FROM termek_keszletek WHERE termek_id = $id AND ";
				$w = array();
				foreach(explode('_', $sor->amid) as $k => $amodid) {
					
					$w[] =  " valtozat".($k+1)."_id = ".$amodid." ";
					
				}
				$sql .= implode(' AND ', $w);
				$rs = $this->Sql->sqlSor( $sql);
				if(isset($rs->keszlet)) {
					$db = $rs->keszlet;
					$keszlet_id = $rs->id;
				} else {
					$db = 0;
					$keszlet_id = 0;
				}
				$adatlista[$kindex]->db = '
						<div class="quantity clearfix">
							<a onclick="aJs.keszletNoveles(this, -1);"  href="javascript:void(0);" title="" class="btn btn-small decrease"></a>
							<input type="text" name="'.($keszlet_id==0?'ujdb['.$sor->amid.']':'db['.$rs->id.']').'" value="'.$db.'">
							<a onclick="aJs.keszletNoveles(this, 1);"  href="javascript:void(0);" title="" class="btn btn-small increase"></a>
						</div>' ;
				
			}
			
			
		}
		
		$tablazat->adatBeallitas('keresoMezok', false);
		$tablazat->adatBeallitas('szerkeszto_url', 'termek/keszletszerkesztes/');
		$tablazat->adatBeallitas('megjelenitettMezok', array('nev' => 'Név', 'db' => 'Készlet'));
		$tablazat->adatBeallitas('lista', $adatlista);
		$tablazat->adatBeallitas('cellaAttr', array('db' => ' class="quantity-cell" '));
		
		$ALG->tartalomDobozVege();
		
		$ALG->urlapGombok(array(
			array('osztaly' => 'btn-ok', 'felirat' => 'Űrlap rögzítése', 'tipus' => 'submit', 'link' => ''),
		));
		$ALG->urlapVege();
		
		return $ALG->kimenet();
	}
	public function kombinaciosLista($valtozatok,  $index = 0) {
		$nevTomb = array();
		foreach($valtozatok[$index] as $sor) {
			if(!empty($valtozatok[$index+1])) {
				$alLista = $this->kombinaciosLista($valtozatok, $index+1);
			}
			if(!empty($alLista)) {
				foreach($alLista as $alSor) {
					$nevTomb[] = (object)array('nev' => $sor->nev.' '.$alSor->nev, 'amid' => $sor->id.'_'.$alSor->amid); 
				}
			} else {
				$nevTomb[] = (object)array('nev' => $sor->nev, 'amid' => $sor->id); 
			}
			
		}
		return $nevTomb;
	}
	public function klonozas() {
		$ci = getCI();
		$this->data['tid'] = $id = (int)$ci->uri->segment(4);
		$termek = $ci->Sql->sqlSor("SELECT * FROM termekek WHERE id = $id");
		
		$armodositok = $ci->Sql->sqlSorok("SELECT * FROM termek_armodositok WHERE termek_id = $id");
		$termekxcimke = $ci->Sql->sqlSorok("SELECT * FROM termekxcimke WHERE termek_id = $id");
		$termekxkategoria = $ci->Sql->sqlSorok("SELECT * FROM termekxkategoria WHERE termek_id = $id");
		$termek_kepek = $ci->Sql->sqlSorok("SELECT * FROM termek_kepek WHERE termek_id = $id");
		$jellemzok = $ci->Sql->sqlSorok("SELECT * FROM jellemzok WHERE termek_id = $id");
		
		$termek = (array)$termek;
		unset($termek['id']);
		$ujid = $this->Sql->sqlSave($termek, 'termekek');
		if($armodositok) foreach($armodositok as $armodosito) {
			$armodosito = (array)$armodosito;
			$armodosito['termek_id'] = $ujid;
			$this->Sql->sqlSave($armodosito, 'termek_armodositok');
		}
		
		if($termekxcimke) foreach($termekxcimke as $sor) {
			$sor = (array)$sor;
			$sor['termek_id'] = $ujid;
			$this->Sql->sqlSave($sor, 'termekxcimke');
		}
		if($jellemzok) foreach($jellemzok as $sor) {
			$sor = (array)$sor;
			$sor['termek_id'] = $ujid;
			$this->Sql->sqlSave($sor, 'jellemzok');
		}
		
		if($termekxkategoria) foreach($termekxkategoria as $sor) {
			$sor = (array)$sor;
			$sor['termek_id'] = $ujid;
			$this->Sql->sqlSave($sor, 'termekxkategoria');
		}
		ws_autoload('termek');
		if($termek_kepek) foreach($termek_kepek as $sor) {
			$sor = (array)$sor;
			$sor['termek_id'] = $ujid;
			
			$kep = file_get_contents(FCPATH.$sor['file']);
			$filename = basename($sor['file']);
			
			$tk = new Termekkep_osztaly();
			$termekMappa = $tk->mappakeszites($ujid);
			
			
			$location = $termekMappa.$filename;
			file_put_contents($location, $kep);
			
			
			$sor['file'] = $location;
			
			$this->Sql->sqlSave($sor, 'termek_kepek');
		}
		
		redirect(ADMINURL.'termek/szerkesztes/'.$ujid);
		
	}
	public function szerkesztes() {
		
		globalisMemoria("Nyitott menüpont",'Termékek');
		$ci = getCI();
		$this->data['tid'] = $id = (int)$ci->uri->segment(4);
		
		if($ci->input->post('a')) {
			$a = $ci->input->post('a');
			if($id==0) {
				$id = $this->sqlSave($a, 'termekek');
			} else {
				$a['id'] = $id;
				$this->sqlUpdate($a, 'termekek', 'id');
			}
			$nyelvek = explode(',', beallitasOlvasas('nyelvek'));
			$foNyelv = current($nyelvek);
			if(isset($_POST['tj'])) {
				foreach($_POST['tj'] as $jId => $tjIds) {
					foreach($tjIds as $tjId => $tjMezo) {
						foreach($tjMezo as $mezo => $ertek) {
							if(is_array($ertek)) {
								
								// szöveges tartalom
								foreach($nyelvek as $nyelv) {
									if(!isset($ertek[$nyelv])) continue;
									
									$sql = "SELECT id FROM jellemzok WHERE  termek_id = {$id} AND nyelv = '$nyelv' AND termek_jellemzo_id = $jId LIMIT 1";
									$letezoSor = $this->sqlSor($sql);
									
									$a = array( $mezo => $ertek[$nyelv], 'termek_id' => $id, 'termek_jellemzo_id' => $jId, 'nyelv' => $nyelv);
									
									if( !isset($letezoSor->id)) {
										$this->sqlSave($a, 'jellemzok', 'id');
									} else {
										$sql = "UPDATE jellemzok SET
											$mezo = ".$this->db->escape($ertek[$nyelv])."
											
											WHERE id = {$letezoSor->id} LIMIT 1";
										$this->db->query($sql);
										
										
									}
								}
							} else {
								
								$a = array( $mezo => $ertek, 'termek_id' => $id, 'termek_jellemzo_id' => $jId);
								 
								if( $tjId == 0) {
									$this->sqlSave($a, 'jellemzok', 'id');
								} else {
									$a['id'] =  $tjId;
									$this->sqlUpdate($a, 'jellemzok', 'id');
								}
							}
						}
					}
				}
			}
			
			if($ci->input->post('opc')) {
				foreach($ci->input->post('opc') as $opcSor) {
					$opcSor['termek_id'] = $id;
					if($opcSor['nev']=='') {
						$this->ci->db->query("DELETE FROM termek_armodositok WHERE id = {$opcSor['id']} LIMIT 1");
						continue;
					}
					if($opcSor['id']==0) {
						
						$this->sqlSave($opcSor, 'termek_armodositok');
					} else {
						$this->sqlUpdate($opcSor, 'termek_armodositok');
						
					}
					
				}
			}
			
			if($ci->input->post('k')) {
				$termekXKategoriaId = array();
				foreach($ci->input->post('k') as $kategoria_id) {
					$a = array('termek_id' => $id, 'kategoria_id' => $kategoria_id);
					$letezoSor = $this->sqlSor("SELECT id FROM termekxkategoria WHERE termek_id = {$a['termek_id']} AND kategoria_id = {$a['termek_id']}");
					if(!isset($letezoSor->id)) {
						$termekXKategoriaId[] = $this->sqlSave($a, 'termekxkategoria', 'id');
						
					} else {
						$termekXKategoriaId[] = $letezoSor->id;
					}
				}
				$this->db->query("DELETE FROM termekxkategoria WHERE termek_id = $id AND id NOT IN (".implode(",", $termekXKategoriaId).")");
			}
			// cimkék
			$valasztottCimkek = $this->ci->Sql->gets("termekxcimke", " WHERE termek_id = {$id} ");
			$checked = array();
			if($valasztottCimkek) {
				foreach($valasztottCimkek as $vc) $checked[$vc->id] = $vc->id;
			}
			if($ci->input->post('cimke')) {
				foreach($ci->input->post('cimke') as  $cimkeId => $v) {
					$sql = "SELECT id FROM termekxcimke WHERE termek_id = $id AND cimke_id = $cimkeId";
					$rs = $this->Sql->sqlSor($sql);
					if(isset($rs->id)) {
						unset($checked[$rs->id]);
					} else {
						// nincs felvive
						$a = array('termek_id' => $id, 'cimke_id' => $cimkeId);
						$this->Sql->sqlSave($a, 'termekxcimke');
					}
				}
			}
			// maradék
			if(!empty($checked)) {
				foreach($checked as $maradekCimke) {
					$this->ci->db->query("DELETE FROM termekxcimke WHERE id = ".$maradekCimke);
				}
			}
			
			
			// képek
			if($ci->uri->segment(4)==0) {
				include_once('osztaly/osztaly_termekkep.php');
				$tk = new Termekkep_osztaly();
				$termekMappa = $tk->kepathelyezes($id);
				
				
			}
			
			
			redirect(ADMINURL.'termek/lista');
			//die();
		}
		
		$this->data['lista'] = $ci->Sql->kategoriaFa(0);
		$sor = $this->data['sor'] = new Termek_osztaly($id);
		$this->data['termekXKategoria'] = $this->getsIdArr('termekxkategoria', 'kategoria_id', ' WHERE termek_id = '.$id.' ');
		
		globalisMemoria('utvonal', array(array('felirat' => 'Termékek', 'url' => 'termek/lista' ), array('felirat' => 'Termékszerkesztés')));
		
		$ALG = new Adminlapgenerator;
		
		$ALG->adatBeallitas('lapCim', "Termék szerkesztése");
		$ALG->adatBeallitas('fejlecGomb', array('url' => ADMINURL.'termek/lista', 'felirat' => 'Vissza'));
		
		$ALG->urlapStart(array('attr' => 'method="post" onsubmit="return false;" id="termekForm" class="termekForm"'));
		$ALG->tartalomDobozStart();
		
		$doboz = $ALG->ujDoboz();
		$doboz->dobozCim("Termék törzsadatok");
		
		$input1 = new Szovegmezo(array('attr'=> ' id="cikkszamertek"  ', 'nevtomb'=>'a', 'mezonev' => 'cikkszam', 'felirat' => 'Cikkszám', 'ertek' => @$sor->cikkszam));
		$gomb = new Urlapgomb(array('attr' => 'class="btn" onclick="aJs.cikkszamGeneralas();" ', 'nevtomb'=>'', 'mezonev' => '', 'felirat' => 'Automatikus cikkszám generálás', 'ertek' => 'Generál'));
		
		$doboz->duplaInput($input1, $gomb);
		
		$input1 = new Szovegmezo(array('attr' => ' onchange="aJs.bruttoSzamitas();" id="arertek" ' ,'nevtomb'=>'a', 'mezonev' => 'ar', 'felirat' => 'Ár (nettó)', 'ertek' => @$sor->ar));
		$input2 = new Szovegmezo(array('attr' => '' ,'nevtomb'=>'a', 'mezonev' => 'eredeti_ar', 'felirat' => 'Eredeti ár (nettó)', 'ertek' => @$sor->eredeti_ar));
		
		$doboz->duplaInput($input1, $input2);
		
		$afak = array();
		$rs = $this->Sql->sqlSorok("SELECT * FROM afaertekek ORDER BY nev ASC");
		foreach($rs as $afasor) $afak[(string)$afasor->ertek] = $afasor->nev;
		
		$select1 = new Legordulo(array('attr' => ' onchange="aJs.bruttoSzamitas();" id="afaertek" ' , 'nevtomb'=>'a', 'mezonev' => 'afa', 'felirat' => 'Áfaosztály', 'ertek' => @$sor->afa, 'opciok' => $afak)) ;
		$brutto = 0;
		if(isset($sor->ar)) {
			if($sor->afa!=0) {
				$brutto = $sor->ar + (($sor->ar/100)*$sor->afa);
			}
		}
		$input = new Szovegmezo(array('nevtomb' => '', 'mezonev' => 'brutto', 'felirat' => 'Bruttó érték', 'ertek' => $brutto, 'attr' => ' onchange="aJs.nettoSzamitas();" id="bruttoertek" '));
		
		$doboz->duplaInput($select1, $input);
		
		$gyartok = array('0' => 'Nincs megadva');
		$rs = $this->Sql->sqlSorok("SELECT * FROM gyartok ORDER BY nev ASC");
		foreach($rs as $gyartosor) $gyartok[(string)$gyartosor->id] = $gyartosor->nev;
		$select1 = new Legordulo(array('attr' => '' , 'nevtomb'=>'a', 'mezonev' => 'gyarto_id', 'felirat' => 'Gyártó', 'ertek' => @$sor->gyarto_id, 'opciok' => $gyartok)) ;
		$select2 = new Legordulo(array('nevtomb'=>'a', 'mezonev' => 'aktiv', 'felirat' => 'Megjelenítve', 'ertek' => @$sor->aktiv, 'opciok' => array('0' => 'NEM', '1' => 'Igen'))) ;
		
		$doboz->duplaInput($select1, $select2);
		
		$doboz->HTMLHozzaadas('<hr><br>'.$this->load->view(ADMINTEMPLATE.'html/jellemzoformbuilder', $this->data, true).'<hr>');
		
		$doboz->HTMLHozzaadas($this->load->view(ADMINTEMPLATE.'html/termekszerkeszto_kategoriak', $this->data, true).'<hr>');
		$this->data['cimkelista'] = $this->ci->Sql->gets("termek_cimkek", " ORDER BY nev ASC, nyelv ASC");
		$valasztottCimkek = $this->ci->Sql->gets("termekxcimke", " WHERE termek_id = {$sor->id} ");
		$checked = array();
		if($valasztottCimkek) {
			foreach($valasztottCimkek as $vc) $checked[$vc->cimke_id] = 1;
		}
		$this->data['checked'] = $checked;
		$doboz->HTMLHozzaadas($this->load->view(ADMINTEMPLATE.'html/termekszerkeszto_cimkek', $this->data, true));
		if(beallitasOlvasas('termek_valtozat_opcio_engedelyezes')=='1') {
			$doboz->HTMLHozzaadas('<hr><div class="valtozat_es_opcio"></div>');
		}
		$doboz->HTMLHozzaadas($this->load->view(ADMINTEMPLATE.'html/termekszerkeszto_kepfeltoltes', $this->data, true));
		$doboz->HTMLHozzaadas($this->load->view(ADMINTEMPLATE.'html/termekszerkeszto_javascript', $this->data, true));
		$doboz->HTMLHozzaadas("<script>var tid = ".$this->data['tid']."</script>");
		
		
		$ALG->tartalomDobozVege();
		
		$ALG->urlapGombok(array(
			array('osztaly' => 'btn-ok', 'onclick' => "document.forms[0].submit();", 'felirat' => 'Űrlap rögzítése', 'tipus' => 'button', 'link' => ''),
		));
		$ALG->urlapVege();
		
		return $ALG->kimenet();
		
	}
	
	// képfeltöltés
	public function imageupload() {
		$tid = $this->ci->uri->segment(4);
		
		include_once('osztaly/osztaly_termekkep.php');
		$tk = new Termekkep_osztaly();
		$termekMappa = $tk->mappakeszites($tid);
		
		
		// postként jön D&D képként?
		if(isset($_POST['file'])) {
			// igen
			$filename = date('YmdHi').rand(1000,9999).'.jpg';
			$location = $termekMappa.$filename;
			
			$imageData = base64_decode(str_replace('data:image/jpeg;base64,', '', $_POST['file']));
			$source = imagecreatefromstring($imageData);
			if(imagejpeg($source, FCPATH.$location)) {
				$a = array('file' => $location, 'termek_id' => $tid);
				$this->Sql->sqlSave($a, 'termek_kepek');
				
				$this->keplista();
				return;
				
			
			}
			
			print 0;
			
			
			return;
		}
		/**
		 * 
		 $file = str_replace('data:image/jpeg;base64,','', $_POST['file']);
			print $file;
			$source = imagecreatefromstring($file);
			if(imagejpeg($source, FCPATH.$location)) {
				$a = array('file' => $location, 'termek_id' => $tid);
				$this->Sql->sqlSave($a, 'termek_kepek');
				
				$this->keplista();
				return;
				
				
			}
			print 0;
			
			return;
		 */
		
		
		$filename = strToUrl(ws_withoutext($_FILES['file']['name'])).'.'.strtolower(ws_ext($_FILES['file']['name']));
		$location = $termekMappa.$filename;
		
		$uploadOk = 1;
		$imageFileType = pathinfo( $_FILES['file']['name'],PATHINFO_EXTENSION);

		// Check image format
		$imageFileType = strtolower($imageFileType);
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
			$uploadOk = 0;
		}
		
		if($uploadOk == 0){
			echo 0;
		}else{
			 /* Upload file */
			 if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
				$a = array('file' => $location, 'termek_id' => $tid);
				$this->Sql->sqlSave($a, 'termek_kepek');
				
				$this->keplista();
			}else{
				echo 0;
			}
		}
		
	}
	// adott levél kéének törlése
	function keptorles() {
		include_once('osztaly/osztaly_termekkep.php');
		
		$kepid = $_POST['kep'];
		
		$tk = new Termekkep_osztaly();
		$tk->kepTorles($kepid);
		
	}
	// adott levél képeinek listázása
	function keplista() {
		include_once('osztaly/osztaly_termekkep.php');
		$tk = new Termekkep_osztaly();
		
		$tid = $this->ci->uri->segment(4);
		$lista = $tk->teljesKeplista($tid);
		foreach($lista as $k => $v) {
			$lista[$k]->file = ws_image($v->file,'mediumboxed');
		}
		
		print json_encode($lista);
	}
}
