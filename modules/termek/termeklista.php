<?php

class Termeklista extends MY_Modul {
	var $rendezesiModok = array('nev','ar', 'nepszeruseg' );
	
	public function __construct() {
		parent::__construct();
		include_once('autoload.php');
		
	}	
	var $order = ' nev ASC ';
	
	
	public function kereses($keresoSzo) {
		ws_autoload('termek');
		$termekListaOsztaly = new Termeklista_osztaly();
		return $termekListaOsztaly->kereses($keresoSzo);
	}
	public function fooldaliszurowidget($param = false) {
		ws_autoload('termek');
		$kategoriaOsztaly = new Kategoriak_osztaly();
		$kategoriak = $kategoriaOsztaly->kategoriaFa();
		
		return $this->ci->load->view(FRONTENDTEMA.'html/fooldalitermekwidget', array('kategoriak' => $kategoriak), true);

	}
	public function fooldalitermekek($param = false) {
		$data = array();
		$db = beallitasOlvasas('termeklista.cimkeszerint.limitdb');
		if(!$db) $db = 5;
		if(isset($param['termekdarab'])) {
			$db = (int)$param['termekdarab'];
		}
		$termekListaOsztaly = new Termeklista_osztaly();
		// mereven kódolom most a cimke id-ket.
		
		$data['akciostermekek'] = $termekListaOsztaly->termekekCimkeszerint(AKCIOSTERMEKCIMKE_ID, $db) ;
		$data['kiemelttermekek'] = $termekListaOsztaly->termekekCimkeszerint(KIEMELTTERMEKCIMKE_ID, $db) ;
		
		return $this->ci->load->view(FRONTENDTEMA.'html/fooldalitermeklista', $data, true);

	}
	public function cimketermeklista($param = false) {
				$maxTalalatszam = (isset($param['talalatszam'])?$param['talalatszam']:0);
		
		$termekListaOsztaly = new Termeklista_osztaly();		if($maxTalalatszam) $termekListaOsztaly->maximumTalalatszam = $maxTalalatszam;
		$uri = $this->ci->uri->segment(2);		
		if(isset($_GET['dbszam'])) {
			$limit = (int)$_GET['dbszam'];
			if($limit < 12 or $limit > 48) $limit = 12;
			 $this->ci->session->set_userdata('termek_listalimit', $limit);
		}
		$limit = (int) $this->ci->session->userdata('termek_listalimit');
		
		if(!$limit) $limit = 12;
		if(isset($_GET['start'])) $start = (int)$_GET['start']; else  $start = 0;
		
		
		if(isset($_GET['rend'])) {
			$rendezes = $_GET['rend'];
			
			if(in_array($rendezes, $this->rendezesiModok)) {
				$this->ci->session->set_userdata('termek_listarendezes', $rendezes);
				$start = 0;
				
				
			}
			 
		}
		$rendezes = $this->ci->session->userdata('termek_listarendezes');
		
		
		
		if(!$rendezes) $rendezes = current($this->rendezesiModok);
		$termekListaOsztaly->rendezesBeallitas($rendezes);
		
		
		
		
		$kategoriak = array();
		$termekek = $termekListaOsztaly->termekekCimkeszerint($param['cimke_id']) ;
		$cimke = $this->Sql->get($param['cimke_id'], "termek_cimkek",'id' );
		$listacim = $cimke->nev;		
		if($maxTalalatszam>0) {
			// csak adott számú terméket jelenítünk meg
			$termekek = array_slice($termekek, 0,$maxTalalatszam); 
		}
		return $this->ci->load->view(FRONTENDTEMA.'html/termeklista', array('maxTalalatszam' => $maxTalalatszam, 'rendezes' => $rendezes,'start' => $start, 'limit' => $limit, 'termekdb' => $termekListaOsztaly->talalatszam, 'termekek' => $termekek, 'kategoriak' => $kategoriak, 'listacim' => $listacim ), true);
		
	}
	
	
	
	public function index($param = false) {
		$termekListaOsztaly = new Termeklista_osztaly();
		$kategoriaOsztaly = new Kategoriak_osztaly();
		$uri = $this->ci->uri->segment(2);		if(isset($_GET['dbszam'])) {
			$limit = (int)$_GET['dbszam'];
			if($limit < 12 or $limit > 48) $limit = 12;
			 $this->ci->session->set_userdata('termek_listalimit', $limit);
		}
		$limit = (int) $this->ci->session->userdata('termek_listalimit');
		
		if(!$limit) $limit = 12;
		if(isset($_GET['start'])) $start = (int)$_GET['start']; else  $start = 0;		
		
		if(isset($_GET['rend'])) {
			$rendezes = $_GET['rend'];
			
			if(in_array($rendezes, $this->rendezesiModok)) {
				$this->ci->session->set_userdata('termek_listarendezes', $rendezes);
				$start = 0;
				
				
			}
			 
		}
		$rendezes = $this->ci->session->userdata('termek_listarendezes');
		
		
		
		if(!$rendezes) $rendezes = current($this->rendezesiModok);
		$termekListaOsztaly->rendezesBeallitas($rendezes);
		
		if($uri=='') {
			$kategoriak = $kategoriaOsztaly->gyermekKategoriak();
			$termekek = $termekListaOsztaly->termekek($limit, $start) ;
		} else {
			$termekek = $termekListaOsztaly->kategoriaTermekek($uri, $limit, $start) ;
			$kategoria = $kategoriaOsztaly->kategoriaSzegmens($uri);
			
			if(empty($termekek) and empty($kategoria)) {
				// TODO:
				// ez csak akkor legyen igaz, ha az aoldalhoz terméklistát társítottunk
				$kategoriak = $kategoriaOsztaly->gyermekKategoriak();
				$termekek = $termekListaOsztaly->termekek($limit, $start) ;	
			} else  {
			
				$kategoriak =  $kategoriaOsztaly->gyermekKategoriak($kategoria->id);
			}
		} 		$view = 'termeklista';
		if(isset($param['view'])) $view = $param['view']; 
		return $this->ci->load->view(FRONTENDTEMA.'html/'.$view, array('rendezes' => $rendezes,'start' => $start, 'limit' => $limit, 'termekdb' => $termekListaOsztaly->talalatszam, 'termekek' => $termekek, 'kategoriak' => $kategoriak ), true);
		
	}
	
	
	public function sliderlista($param) {
		$termekLista = new Termeklista_osztaly();
		return $this->ci->load->view(FRONTENDTEMA.'html/kiemeltlista', array( 'tipus' => $param['tipus'], 'lista' => $termekLista->kiemeltTermekek($param['tipus'], $param['termek_db'])), true);
		
	}
	public function termeklap() {
		$termekLista = new Termeklista_osztaly();
		$termekUrl = $this->ci->uri->segment(2);
		if($termekUrl=='') {
			redirect('404');
		}
		$adat =  explode('-', $termekUrl);
		
		$id = (int)$adat[0];
		$termek = new Termek_osztaly($id);
		
		
		return $this->ci->load->view(FRONTENDTEMA.'html/termeklap', array('termek' => $termek), true);
		
	}
}
