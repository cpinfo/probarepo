<?php
class Import_admin extends MY_Modul {
	var $data = array();
	
	public function index() {
		globalisMemoria("Nyitott menüpont",'Tartalomkezelés');
		

		globalisMemoria('utvonal', array(array('felirat' => 'Tartmék importálás')));

		$ALG = new Adminlapgenerator;
		$ALG->adatBeallitas('lapCim', "Importálás");
		
		$ALG->adatBeallitas('fejlecGomb', array('url' => ADMINURL.'import/start/1', 'felirat' => 'START'));
		
		$ALG->tartalomDobozVege();

		return $ALG->kimenet();
		
		
	}
	public function start() {
		
		ws_autoload('termek');
		// implementálni lehet itt a kül. forrásokat, ez most csak egy példa az import/update-ra
		
		$xml = simplexml_load_file(FCPATH.'assets/feed/files-products-xml-standard-product-2501-hu.xml');
		
		foreach($xml as $sor) {
			$t = array();
			$t['cikkszam'] = 'imp'.$sor->id;
			$t['ar'] = ((float)$sor->price)*300;
			$t['aktiv'] = 1;
			
			$sql = "SELECT * FROM termekek WHERE cikkszam = '{$t['cikkszam']}' LIMIT 1";
			$van = $this->Sql->sqlSor($sql);
			if($van) {
				// update
				$t['id'] = $van->id;
				$this->Sql->sqlUpdate($t, 'termekek');
				
			} else {
				$t['id'] = $this->Sql->sqlSave($t, 'termekek');
				
			}
			
			// kategórizálás, most csak egyszerűen beteszem egy főkategória alá
			$sql = "SELECT * FROM kategoriak WHERE slug = 'importkategoria' ";
			$vanKat = $this->Sql->sqlSor($sql);
			
			if($vanKat) {
				$katId = $vanKat->id;
				$vanKatXTerm = $this->Sql->sqlSor("SELECT * FROM termekxkategoria WHERE kategoria_id = ".$katId." AND termek_id = {$t['id']}" );
				if(!$vanKatXTerm) {
					// nincs még fenn a kategória kapcsolat
					$k = array('termek_id' => $t['id'], 'kategoria_id' => $katId);
					$this->sqlSave($k, 'termekxkategoria');
				}
			} else {
				$katId = $this->sqlSave(array('slug' => 'importkategoria','nev' => 'Importált termékek','kep' => 'https://i.imgur.com/uSxT5U9.png' ), 'kategoriak');
				$k = array('termek_id' => $t['id'], 'kategoria_id' => $katId);
				$this->sqlSave($k, 'termekxkategoria');
			}
			//print '**';
			$this->termekJellemzo('Név' ,$t['id'], JELLEMZO_STRING,  (string)$sor->name);
			$this->termekJellemzo('Leírás' ,$t['id'], JELLEMZO_TEXT,  (string)$sor->description);
			if((string)$sor->attribute1!='') {
				$this->termekJellemzo((string)$sor->attribute1 ,$t['id'], JELLEMZO_STRING,  (string)$sor->value1);
			}
			if((string)$sor->attribute2!='') {
				$this->termekJellemzo((string)$sor->attribute2 ,$t['id'], JELLEMZO_STRING,  (string)$sor->value2);
			}
			
			
			if((string)$sor->video!='') {
				$video = (string)$sor->video;
				
				if($video=='0') $video = '';
				print $video.' '; 
				$this->termekJellemzo('Youtube' ,$t['id'], JELLEMZO_STRING,  $video);
			}
			if((string)$sor->width!='') {
				$this->termekJellemzo('Szélesség' ,$t['id'], JELLEMZO_LEBEGO,  (string)$sor->width);
			}
			if((string)$sor->height!='') {
				$this->termekJellemzo('Magasság' ,$t['id'], JELLEMZO_LEBEGO,  (string)$sor->height);
			}
			if((string)$sor->depth!='') {
				$this->termekJellemzo('Mélység' ,$t['id'], JELLEMZO_LEBEGO,  (string)$sor->depth);
			}
			if((string)$sor->weight!='') {
				$this->termekJellemzo('Súly' ,$t['id'], JELLEMZO_LEBEGO,  (string)$sor->weight);
			}
			
			
			
			//print $t['id']." -- ".$this->termekJellemzo('Név' ,$t['id'], JELLEMZO_STRING).' ';
			
			// képek...
			$termekKep = new Termekkep_osztaly();
			
			$dir = $termekKep->mappakeszites($t['id']);
			$fokep = true;
			
			for($i=0; $i<9;$i++) {
				$mezo = 'image'.$i;
				if((string)$sor->$mezo!='') {
					$file_nev = basename($sor->$mezo);
					if(!file_exists(FCPATH.$dir.$file_nev)) {
						if(file_put_contents(FCPATH.$dir.$file_nev, file_get_contents($sor->$mezo))) {
							// mentés vagy update
							$a = array(
								'szerep' => ($fokep?1:0),
								'file' => $dir.$file_nev, 
								'termek_id' => $t['id']);
							
							$this->Sql->sqlSave($a, 'termek_kepek');
							$fokep = false;
						}
					}
				}
			}
			
		}
		return '<h2>KÉSZ</h2>';
		
	}
	
	function termekJellemzo($kulcs, $tid, $tipus = null, $ertek = null ) {
		
		$sql = "SELECT id FROM termek_jellemzok WHERE nev = '$kulcs' LIMIT 1";
		$vanKulcs = $this->Sql->sqlSor($sql);
		
		if(!$vanKulcs) {
			$a = array('nev' => $kulcs, 'tipus' => $tipus);
			$kulcsId = $this->sqlSave($a, 'termek_jellemzok' );
		} else {
			$kulcsId = $vanKulcs->id;
		}
		
		$sql = "SELECT id, ertek_{$tipus} as ertek FROM jellemzok WHERE termek_jellemzo_id = $kulcsId AND termek_id = $tid LIMIT 1";
		$van = $this->Sql->sqlSor($sql);
		
		if($van) {
			
			if($ertek===null) return $van->ertek;
			
			// update
			$a = array('id' => $van->id,'termek_id' => $tid,  'termek_jellemzo_id' => $kulcsId, 'ertek_'.$tipus => $ertek);
			
			
			$this->sqlUpdate($a, 'jellemzok');
		} else {
			if($ertek===null) return false;
			// update
			$a = array( 'termek_jellemzo_id' => $kulcsId, 'termek_id' => $tid, 'ertek_'.$tipus => $ertek);
			$this->sqlSave($a, 'jellemzok');
		}
		
		
	}
}

