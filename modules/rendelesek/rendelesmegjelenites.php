<?php

class Rendelesmegjelenites extends MY_Modul {
	
	function lista(){
		
		$tag = ws_belepesEllenorzes();
		if(!$tag) {
			redirect(base_url());
			return;
		}
		$start = 0;
		$limit = 2000;  // TODO lapozó a rendelésekhez
		$w = " WHERE r.rendeles_felhasznalo_id = f.id AND f.felhasznalo_id = ".$tag->id;
		$lista = $this->sqlSorok('SELECT r.id as rendelesid, r.* FROM rendelesek r, rendeles_felhasznalok f  '.$w.' ORDER BY ido DESC LIMIT '.$start.', '.$limit);

		$statuszopciok = '';

		$statuszArr = array();

		foreach($this->gets("rendeles_statusz", " ORDER BY sorrend ASC") as $sor)  {

			$statuszopciok .= '<option  value="'.$sor->id.'">'.$sor->nev.'</option>';

			$statuszArr[$sor->id] = $sor->nev;

			

		}

		foreach($lista as $sor) {

			$sor->statusz = $statuszArr[$sor->statusz];

			$vevoRs = $this->Sql->sqlSor("SELECT * FROM rendeles_felhasznalok WHERE id = {$sor->rendeles_felhasznalo_id} LIMIT 1");

			$sor->vevo = $vevoRs->vezeteknev.' '.$vevoRs->keresztnev;

			$sor->osszar = ws_arformatum($sor->osszbrutto).' Ft';

			$sor->ido = date('Y-m-d', strtotime($sor->ido));

		}
		
		return $this->ci->load->view(FRONTENDTEMA.'html/rendeleseim', array('lista' => $lista), true);
	}
}
